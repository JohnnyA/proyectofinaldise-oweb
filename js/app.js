
 /**
  * Guarda a un nuevo usuario en el localStorage
  */


 function guardarUsuario(){
   let nombre = document.getElementById('nombre-registro').value;
   let apellido = document.getElementById('apellido-registro').value;
   let telefono = document.getElementById('telefono-registro').value;
   let nombreUsuario = document.getElementById('nombreUsuario-registro').value;
   let pass = document.getElementById('rePass-registro').value;
   let velocidad = "";
   let sobreMi="";

   const usuario = {
     nombre,
     apellido,
     telefono,
     nombreUsuario,
     pass,
     velocidad,
     sobreMi
   };
   return usuario;
 }



 /**
  * guarda los datos necesarios en el sessionStorage
  */
 function datosUsuarioSession() {
   let nombreUsuario = document.getElementById('usuario-sesion').value;
   let idUsuario= obtenerIdUsuario('usuarios',nombreUsuario);

   const usuarioSession={
     nombreUsuario,
     idUsuario
   };
   const usuariosSession = guardarSession('usuario_session',usuarioSession);
 }



 /**
  * Obtiene a un usuaio de una tabla
  */
 function obtenerUsuario(tableName) {
   let nombreUsuario="";
   let session= obtenerDatosSession(tableName);
   for(let i in session){
     nombreUsuario= session[i].nombreUsuario;
     break;
   }
   return nombreUsuario;
 }

 /**
  * Obtiene el id de una tabla de usuarios
  * @param nombreTabla nombre de la tabla
  */
 function obtenerIdUsuario(nombreTabla,nombreUsuario){
   let usuarios=obtenerDatosTabla(nombreTabla);
   let id="";
   for(let i in usuarios){
     if(nombreUsuario==usuarios[i].nombreUsuario){
       id=usuarios[i].id;
       break;
     }
   }
   return id;
 }

/**
 * metodos de localStorage y sessionStorage
 */

 /**
 


 /**
  * Inserta los datos del usuaroio en la tabla usuario del localStorage
  *
  * @param {*} nombreTabla nombre de la tabla donde se insertará un objeto
  * @param {*} objeto objeto que se inserta en la tabla
*/
  function insertarEnTabla(tableName, object) {
    let tableData = JSON.parse(localStorage.getItem(tableName));

    if (!tableData) {
      tableData = [];
    }
    let primaryKey = tableData.length + 1;
    object.id = primaryKey;
    tableData.push(object);
    localStorage.setItem(tableName, JSON.stringify(tableData));
    return tableData;
  }  

  /**
   * Obtener datos de usuario
   *
   * @param {*} tableName nombre de la tabla
   */
  function obtenerDatosTabla(tableName) {
    let tableData = JSON.parse(localStorage.getItem(tableName));

    if (!tableData) {
      tableData = [];
    }
    return tableData;
  }

  /**
   *
   * @param {*} llave llave para guardar
   * @param {*} valor valor asociado a la llave
   */
  function guardarLocalStorage(llave, valor){
    localStorage.setItem(key, JSON.stringify(valor));
    return true;
  }

 /**
  * Inserta cualquier objeto en la tabla del sessionStorage
  *
  * @param {*} nombreTabla nombre de la tabla donde se insertará un objeto
  * @param {*} objeto objeto que se inserta en la tabla
  */
function guardarSession(tableName, object){
  let tableData = JSON.parse(sessionStorage.getItem(tableName));

  if (!tableData) {
    tableData = [];
  }
  let primaryKey = tableData.length + 1;
  object.id = primaryKey;
  tableData.push(object);
  sessionStorage.setItem(tableName, JSON.stringify(tableData));
  return tableData;
}

/**
 * obtiene los datos de una tabla en el sessionStorage
 * @param tableName nombre de la tabla
 */
function obtenerDatosSession(tableName) {
  let tableData = JSON.parse(sessionStorage.getItem(tableName));

  if (!tableData) {
    tableData = [];
  }
  return tableData;
}

/**
 * Metodos login
 */

 function validarUsuario(){
   let usuario = document.getElementById('usuario-sesion').value;
   let pass = document.getElementById('pass-sesion').value;
   let usuariosS = obtenerDatosTabla('usuarios');

   for(let i in usuariosS){
     if (usuario==usuariosS[i].nombreUsuario && pass==usuariosS[i].pass) {
       window.location.href="dashboard.html";
       break;
     }
   }
 }

 function verificarRegistro(){
   let nombreUsuario = document.getElementById('nombreUsuario-registro').value;
   let usuarios=obtenerDatosTabla('usuarios');
   let user="";
   for(let i in usuarios){
     if(nombreUsuario==usuarios[i].nombreUsuario){
       user=usuarios[i].nombreUsuario;
       break;
     }
   }
   let usuario= guardarUsuario();
   if(!usuario.nombre || !usuario.apellido || !usuario.telefono ||
   !usuario.nombreUsuario || !usuario.pass || !document.getElementById('rePass-registro').value){
     window.alert("Por favor llenar todos los campos");
   }else{
     if(document.getElementById('pass-registro').value==document.getElementById('rePass-registro').value){
       if(user!=nombreUsuario){
         usuario=insertarEnTabla('usuarios', usuario);
       }else {
         window.alert("el usuario ya existe");
       }
      }else {
        window.alert("las contraseñas no coinciden");
      }
   }
}


function verificarCamposInicio(campoUno, campoDos, nombreTabla, datosTabla){
  if(!campoUno || !campoDos){
    window.alert("No olvides llenar los campos");
  }else{
    renderizarTablaBusqueda(campoUno, campoDos, nombreTabla, datosTabla);
  }
}

/**
 * limpia el modal del registro
 */
function limpiarModal(){
  jQuery('.modal-btn').on('click', function() {
 		document.getElementById('nombre-registro').value="";
    document.getElementById('apellido-registro').value="";
    document.getElementById('telefono-registro').value="";
    document.getElementById('nombreUsuario-registro').value="";
    document.getElementById('pass-registro').value="";
    document.getElementById('rePass-registro').value="";
 	});
}

function limpiarRide(){
  jQuery('#btn-guardar-ride').on('click',function(){
    document.getElementById('nombreRide').value="";
    document.getElementById('salidaRide').value="";
    document.getElementById('destinoRide').value="";
    document.getElementById('descripcionRide').value="";
    document.getElementById('horaSalidaRide').value="";
    document.getElementById('horaLlegadaRide').value="";

  });
}

/**
 * limpia el sessionStorage cuando cierra la sesión
 */
function cerrarSesion(){
  window.location.href="Inicio.html";
    sessionStorage.clear();
}

 function eventos() {
 	jQuery('#agregar-usuario-button').bind('click', (element) => {
 		verificarRegistro();
 	});
  jQuery('#btn-sesion').bind('click',(element) =>{
    validarUsuario();
    datosUsuarioSession();
  });
  limpiarRide();
  limpiarModal();
 }

 eventos();
